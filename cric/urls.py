from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from cric import views


urlpatterns = [
    path('stadiums/', views.StadiumList.as_view()),
    path('stadiums/<str:stadium_name>', views.StadiumDetails.as_view()),
    path('series/', views.SeriesList.as_view()),
    path('series/<str:series_name>', views.SeriesDetails.as_view()),
    path('teams/', views.TeamList.as_view()),
    path('teams/<str:team_name>', views.TeamDetails.as_view()),
    path('matches/', views.MatchList.as_view()),
    path('matches/<int:match_no>/', views.MatchDetails.as_view()),
    path('players/', views.PlayerList.as_view()),
    path('players/<str:name>/', views.PlayerDetails.as_view()),
]
urlpatterns = format_suffix_patterns(urlpatterns)

