from django.db import models


class Stadium(models.Model):
    stadium_name = models.CharField(max_length=30, primary_key=True)
    stadium_location = models.CharField(max_length=20)

    def __str__(self):
        return self.stadium_name

    class Meta:
        ordering = ('stadium_name',)


class Team(models.Model):
    team_name = models.CharField(max_length=20, primary_key=True)
    captain = models.ForeignKey('Player', on_delete=models.CASCADE, related_name='captain_get', null=True, blank=True)

    def __str__(self):
        return self.team_name

    class Meta:
        ordering = ('team_name',)


class Player(models.Model):
    role_batsman = 'Batsman'
    role_bowler = 'Bowler'
    role_all = 'All Rounder'
    role_wicket = 'Wicket Keeper'
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    name = models.CharField(max_length=20, primary_key=True)
    age = models.IntegerField()
    role_choices = (
        (role_wicket, 'Wicket Keeper'),
        (role_batsman, 'Batsman'),
        (role_bowler, 'Bowler'),
        (role_all, 'All Rounder'),
    )
    role = models.CharField('Role', max_length=20, choices=role_choices)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class Series(models.Model):
    series_name = models.CharField(max_length=20, primary_key=True)
    matches = models.IntegerField()
    stadium = models.ManyToManyField(Stadium)
    team = models.ManyToManyField(Team, related_name='team_set')
    winner = models.ForeignKey(Team, on_delete=models.CASCADE)

    def __str__(self):
        return self.series_name

    class Meta:
        ordering = ('series_name',)


class Match(models.Model):
    series = models.ForeignKey(Series, on_delete=models.CASCADE)
    match_no = models.IntegerField(primary_key=True)
    stadium = models.ForeignKey(Stadium, on_delete=models.CASCADE)
    team = models.ManyToManyField(Team, related_name='team_get')
    toss_winner = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='toss_winner_set')
    match_winner = models.ForeignKey(Team, on_delete=models.CASCADE)
    man_of_the_match = models.ForeignKey(Player, on_delete=models.CASCADE)

    class Meta:
        ordering = ('match_no',)

