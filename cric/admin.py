from django.contrib import admin
from django.contrib.auth.models import Group
from .models import Team, Player, Series, Stadium, Match


class PlayerAdmin(admin.ModelAdmin):
    list_display = ['team', 'name', 'age', 'role']
    list_filter = ['team']
    search_fields = ['name']


class TeamAdmin(admin.ModelAdmin):
    list_display = ['team_name', 'captain']
    list_filter = ['team_name']
    search_fields = ['team_name']


class SeriesAdmin(admin.ModelAdmin):
    list_display = ['series_name', 'matches', 'stadiums', 'teams', 'winner']
    list_filter = ['series_name', 'stadium']
    search_fields = ['series_name']

    def teams(self, obj):
        return ", ".join([p.team_name for p in obj.team.all()])

    def stadiums(self, obj):
        return ", ".join([p.stadium_name for p in obj.stadium.all()])


class StadiumAdmin(admin.ModelAdmin):
    list_display = ['stadium_name', 'stadium_location']
    list_filter = ['stadium_name', 'stadium_location']
    search_fields = ['stadium_name', 'stadium_location']


class MatchAdmin(admin.ModelAdmin):
    list_display = ['series', 'match_no', 'stadium', 'teams', 'toss_winner', 'man_of_the_match', 'match_winner']
    list_filter = ['stadium']
    search_fields = ['teams']

    def teams(self, obj):
        return ", ".join([p.team_name for p in obj.team.all()])


admin.site.unregister(Group)
admin.site.register(Team, TeamAdmin)
admin.site.register(Player, PlayerAdmin)
admin.site.register(Series, SeriesAdmin)
admin.site.register(Stadium, StadiumAdmin)
admin.site.register(Match, MatchAdmin)


